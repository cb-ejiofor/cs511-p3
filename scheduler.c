/*
	Author:  Chidubem Bryan Ejiofor

*/


#include<stdio.h>
#include<stdlib.h>
#include<string.h>


struct node {
	int pid;
	int n;
	struct node *next;
};


struct list {
	struct node *head;
	char *name;
};


//print out the list
void print(struct list *thislist){
	printf("%s->",thislist->name);
	struct node * current = thislist->head;
	while(current!=NULL){
		printf("%d->", current->n);
		current=current->next;
	}
	printf("\n");
}


//insert the newnode at the end of the list
void insert(struct list *thislist, struct node* newnode){

        struct node *current = thislist->head;

	//if the list is empty
	//head points to the new node (the only node)
        if(current == NULL){
                thislist->head = newnode;
                return;
        }

	
	//find the last node
        while(current->next!=NULL){
                current = current->next;
        }

	//the last node -> next points to the new node
        current->next = newnode;
        return;


}

//remove a node from the list (first in first out)
struct node *  dequeue(struct list *thislist){
	if(thislist->head==NULL){
		return NULL;
	}
	else {
		struct node *tmp = thislist->head;
		thislist->head=thislist->head->next;
		return tmp;
	}
}

void schedule(struct list mylist[3], struct node tmpNode, int k) {
	int rt0 = tmpNode->n - 8;
	int rt1 = tmpNode->n - 16;
	struct node* newnode = malloc(sizeof(struct node));
	if (k == 0 && (rt0 > 0)) {
		newnode->pid = tmpNode->pid;
		newnode->n = rt0;
		newnode->next = NULL;

		insert(mylist[k+1], newnode);
	}
	else if (k == 1 && (rt1 > 0)) {
		newnode->pid = tmpNode->pid;
		newnode->n = rt1;
		newnode->next = NULL;

		insert(mylist[k+1], newnode);
	}
}

int main(int argc, char* argv[]) {
	struct list* mylist[3];
	int processCount = 0;
	for (int k = 0; k < 3; k++) {
		mylist[k] = malloc(sizeof(struct list));
		mylist[k]->head = malloc(sizeof(struct node));
		mylist[k]->name = malloc(sizeof(char*));


		mylist[k]->head = NULL;

		if(k == 0)
			mylist[k]->name = "Q0";
		if (k == 1)
			mylist[k]->name = "Q1";
		if (k == 2)
			mylist[k]->name = "FCFS";

		int n;

		//read in the number of processes	
		scanf("%d", &n);

		//read in the burst time
		int i;
		for (i = 0; i < n; i++) {
			struct node* tmp = malloc(sizeof(struct node));
			tmp->pid = processCount;
			scanf("%d", &(tmp->n));
			tmp->next = NULL;
			insert(mylist[k], tmp);
			processCount++;
		}
	}

	for (int i = 0; i < 3; i++)
		print(mylist[i]);

	for (int k = 0; k < 3; k++)
	{
		//FCFS
		while (1) {
			//print(mylist);
			struct node* tmp = dequeue(mylist[k]);
			if (tmp == NULL) {
				break;
			}
			printf("P%d(%d)\n", tmp->pid, tmp->n);

			schedule(mylist, tmp, k);

			for (int i = 0; i < 3; i++)
				print(mylist[i]);

			free(tmp);
		}
	}

	for (int k = 0; k < 3; k++)
		free(mylist[k]);

	return 0;
}
